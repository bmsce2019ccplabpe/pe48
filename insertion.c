#include<stdio.h>
int main ()
{
  int a[20], i, pos, ele=0, n;
  printf ("Enter the size of the array\n");
  scanf ("%d", &n);
  printf ("Enter the elements of the array\n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  printf ("Enter the element to be inserted and position\n");
  scanf ("%d%d", &ele, &pos);
  for (i = n - 1; i >= pos; i--)
    a[i + 1] = a[i];
  a[pos] = ele;
  n++;
  printf ("The array after insertion is  ");
  for (i = 0; i < n; i++)
    printf ("%d  ", a[i]);
  return 0;
}